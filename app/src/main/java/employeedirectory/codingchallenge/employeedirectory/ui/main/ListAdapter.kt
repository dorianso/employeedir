package employeedirectory.codingchallenge.employeedirectory.ui.main

import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import employeedirectory.codingchallenge.employeedirectory.R
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee


class ListAdapter(employees: List<Employee>, private val listener: RowClickListener<Employee>) : RecyclerView.Adapter<ListAdapter.EmployeeViewHolder>() {

    var allEmployees = employees.toMutableList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class EmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val employeeName: TextView = itemView.findViewById(R.id.name)
        val employeeTitle: TextView = itemView.findViewById(R.id.title)
        val employeeDepartment: TextView = itemView.findViewById(R.id.department)
        val view = itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return EmployeeViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val current = allEmployees[position]
        holder.employeeName.text = "${current.firstName} ${current.lastName}"
        holder.employeeDepartment.text = current.department
        holder.employeeTitle.text = current.title
        holder.view.setOnClickListener{
            listener.onItemClicked(holder.adapterPosition, current)
        }

    }

    override fun getItemCount(): Int {
        return allEmployees.size
    }
}