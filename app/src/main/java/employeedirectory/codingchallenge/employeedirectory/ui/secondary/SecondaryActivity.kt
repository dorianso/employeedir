package employeedirectory.codingchallenge.employeedirectory.ui.secondary

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import employeedirectory.codingchallenge.employeedirectory.R
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

class SecondaryActivity : AppCompatActivity() {

    val frag = SecondaryFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.secondary_acitivity)


        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, frag)
        transaction.commit()

//        frag.setEmployeeData(getEmployeeInfo())

    }

    public fun getEmployeeInfo(): Employee {

        val firstName = intent.extras.getString("firstName")
        val lastName = intent.extras.getString("lastName")
        val address = intent.extras.getString("address")
        val city = intent.extras.getString("city")
        val department = intent.extras.getString("department")
        val email = intent.extras.getString("email")
        val gender = intent.extras.getString("gender")
        val phone = intent.extras.getString("phone")
        val title = intent.extras.getString("title")

        return Employee(firstName, lastName, department, title, email, phone, gender, address, city)

    }

}