package employeedirectory.codingchallenge.employeedirectory.ui.secondary

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import employeedirectory.codingchallenge.employeedirectory.R
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

class SecondaryFragment :  Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.secondary_fragment, container, false)
        return root
    }

    public fun setEmployeeData(employee : Employee, view : View){
        val name = view.findViewById<TextView>(R.id.firtslastname)
        val title = view.findViewById<TextView>(R.id.title)
        val department = view.findViewById<TextView>(R.id.department)
        val phone = view.findViewById<TextView>(R.id.phone)
        val email = view.findViewById<TextView>(R.id.email)
        val location = view.findViewById<TextView>(R.id.location)
        val city = view.findViewById<TextView>(R.id.city)
        val image = view.findViewById<ImageView>(R.id.imageView)

        name.text = "${employee.firstName} ${employee.lastName}"
        title.text = employee.title
        department.text = employee.department
        phone.text = employee.phone
        email.text = employee.email
        location.text = employee.address
        city.text = employee.city
        if(employee?.gender.equals("Female")){
            image.setImageDrawable(resources.getDrawable(R.drawable.female))
        }
        else if(employee?.gender.equals("Male")){
            image.setImageDrawable(resources.getDrawable(R.drawable.male))
        }

    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val act = activity as SecondaryActivity
        setEmployeeData(act.getEmployeeInfo(), view)
    }
}