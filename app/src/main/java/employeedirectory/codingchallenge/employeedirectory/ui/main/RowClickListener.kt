package employeedirectory.codingchallenge.employeedirectory.ui.main

interface RowClickListener<T> {
    fun onItemClicked(position: Int, t: T)
}