package employeedirectory.codingchallenge.employeedirectory.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import employeedirectory.codingchallenge.employeedirectory.EmployeeContract
import employeedirectory.codingchallenge.employeedirectory.R
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee
import employeedirectory.codingchallenge.employeedirectory.ui.secondary.SecondaryActivity
import employeedirectory.codingchallenge.employeedirectory.ui.secondary.SecondaryFragment


class MainFragment : Fragment(), EmployeeContract.View {
    private lateinit var mPresenter: EmployeeContract.Presenter
    private lateinit var mReyclerView: RecyclerView


    private val mClickListener = object : RowClickListener<Employee> {
        override fun onItemClicked(position: Int, t: Employee) {
            mPresenter.onClickedRow(position, t)
        }

    }
    private  val mAdapter: ListAdapter = ListAdapter(ArrayList(0), mClickListener)

    override fun setPresenter(presenter: EmployeeContract.Presenter) {
        mPresenter = presenter
    }

    override fun showAllEmployees(employees: List<Employee>) {
        mAdapter.allEmployees = employees.toMutableList()
    }

    override fun showEmployeeDetail(employee: Employee) {
        val intent = Intent(context, SecondaryActivity::class.java)
        intent.putExtra("firstName", employee.firstName)
        intent.putExtra("lastName", employee.lastName)
        intent.putExtra("address", employee.address)
        intent.putExtra("city", employee.city)
        intent.putExtra("department", employee.department)
        intent.putExtra("email", employee.email)
        intent.putExtra("gender", employee.gender)
        intent.putExtra("phone", employee.phone)
        intent.putExtra("title", employee.title)

        startActivity(intent)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView = inflater.inflate(R.layout.main_fragment, container, false)
        mReyclerView = rootView.findViewById(R.id.recyclerView)
        mReyclerView.adapter = mAdapter
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayout.VERTICAL)
        mReyclerView.addItemDecoration(dividerItemDecoration)
        mReyclerView.layoutManager = LinearLayoutManager(activity)
        return rootView
    }


    override fun onResume() {
        super.onResume()
        mPresenter.start()
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}
