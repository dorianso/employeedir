package employeedirectory.codingchallenge.employeedirectory.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

@Dao
interface EmployeeDao {

    @Query("SELECT * FROM employeedirectory")
    fun getAllEmployees(): List<Employee>

    @Query("SELECT * FROM EmployeeDirectory WHERE id = :id")
    fun getEmployee(id: Long): Employee

    @Insert
    fun insert(vararg employee: Employee)

    @Query("DELETE FROM employeedirectory")
    fun deleteAll()
}