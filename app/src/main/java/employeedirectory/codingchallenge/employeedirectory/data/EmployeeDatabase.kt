package employeedirectory.codingchallenge.employeedirectory.data

import android.content.Context
import androidx.room.RoomDatabase
import androidx.room.Database
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee
import androidx.room.Room
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import employeedirectory.codingchallenge.employeedirectory.MainActivity
import employeedirectory.codingchallenge.employeedirectory.R
import employeedirectory.codingchallenge.employeedirectory.utils.RunBlock
import com.google.gson.reflect.TypeToken

@Database(entities = arrayOf(Employee::class), version = 1)
abstract class EmployeeRoomDatabase : RoomDatabase() {

    abstract fun employeeDao(): EmployeeDao

    companion object {

        private var INSTANCE: EmployeeRoomDatabase? = null

        internal fun getDatabase(context: Context, callback: MainActivity.DatabaseCallback): EmployeeRoomDatabase {
            if (INSTANCE == null) {
                synchronized(EmployeeRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                EmployeeRoomDatabase::class.java, "employee_database")
                                .addCallback(object : RoomDatabase.Callback() {
                                    override fun onCreate(db: SupportSQLiteDatabase) {
                                        initDatabaseOnCreation(context)
                                        callback.onCompleted()
                                    }

                                    override fun onOpen(db: SupportSQLiteDatabase) {
                                        //no op
                                    }
                                })
                                .build()

                    }
                }
            }
            return INSTANCE!!
        }

        private fun initDatabaseOnCreation(context: Context) {
            RunBlock.getInstance().onIOThread {
                val text = context.resources.openRawResource(R.raw.employeedirectory)
                        .bufferedReader().use { it.readText() }

                val gson = Gson()
                val listType = object : TypeToken<List<Employee>>() {}.type
                val employee : List<Employee> = gson.fromJson(text, listType)
                for (i in employee){
                    INSTANCE!!.employeeDao().insert(i)
                }
            }
        }

    }
}