package employeedirectory.codingchallenge.employeedirectory.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "EmployeeDirectory")
data class Employee(
        @SerializedName("first_name") var firstName: String,
        @SerializedName("last_name") var lastName: String,
        @SerializedName("department") var department: String?,
        @SerializedName("jobTitle") var title: String,
        @SerializedName("email") var email: String?,
        @SerializedName("phone") var phone: String?,
        @SerializedName("gender") var gender: String?,
        @SerializedName("address") var address: String?,
        @SerializedName("city") var city: String?){
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
