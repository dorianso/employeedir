package employeedirectory.codingchallenge.employeedirectory.data

import android.app.Application
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee
import employeedirectory.codingchallenge.employeedirectory.utils.RunBlock

class LocalDataSource( dao : EmployeeRoomDatabase) : DataSource {

    private val daoEmployees: EmployeeDao = dao.employeeDao()

    private val runThisBlock
        get() = RunBlock.getInstance()


    override fun getAllEmployees(loadEmployeesCallback: DataSource.LoadEmployeesCallback) {
        runThisBlock.onIOThread {
            val list = daoEmployees.getAllEmployees()
            runThisBlock.onMainThread { loadEmployeesCallback.onEmployeesLoaded(list) }
        }
    }

    override fun insertEmployee(employee: Employee) {
       runThisBlock.onIOThread { daoEmployees.insert(employee) }
    }

}