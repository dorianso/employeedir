package employeedirectory.codingchallenge.employeedirectory.data

import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

interface DataSource {

    interface LoadEmployeesCallback{
        fun onEmployeesLoaded(list: List<Employee>)
    }
    fun getAllEmployees(loadEmployeesCallback: LoadEmployeesCallback)

    fun insertEmployee(employee: Employee)
}