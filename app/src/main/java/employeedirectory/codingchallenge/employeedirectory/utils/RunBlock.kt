package employeedirectory.codingchallenge.employeedirectory.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class RunBlock private constructor(private val io: Executor = Executors.newSingleThreadExecutor(),
                                   private val mainHandler: Handler = Handler(Looper.getMainLooper())) {

    fun onIOThread(function: () -> Unit) {
        io.execute { function() }
    }

    fun onMainThread(function: () -> Unit) {
        mainHandler.post { function() }
    }

    companion object {
        private var INSTANCE: RunBlock? = null

        private val lock = Any()

        fun getInstance(): RunBlock {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = RunBlock()
                }
                return INSTANCE!!
            }
        }
    }

}