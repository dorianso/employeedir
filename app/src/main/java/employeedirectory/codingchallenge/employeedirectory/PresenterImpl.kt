package employeedirectory.codingchallenge.employeedirectory

import employeedirectory.codingchallenge.employeedirectory.data.DataSource
import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

class PresenterImpl(private val mView: EmployeeContract.View, private val mDataSource: DataSource) : EmployeeContract.Presenter{

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadAllEmployees()
    }

    override fun loadAllEmployees() {
        mDataSource.getAllEmployees(object : DataSource.LoadEmployeesCallback{
            override fun onEmployeesLoaded(list: List<Employee>) {
                mView.showAllEmployees(list)
            }
        })
    }

    override fun onClickedRow(position: Int, employee: Employee) {
            mView.showEmployeeDetail(employee)
    }
}