package employeedirectory.codingchallenge.employeedirectory

import employeedirectory.codingchallenge.employeedirectory.data.entities.Employee

interface EmployeeContract{

    interface View {
        fun showAllEmployees(employees : List<Employee>)
        fun showEmployeeDetail(employee: Employee)
        fun setPresenter(presenter: Presenter)
    }

    interface Presenter{
        fun loadAllEmployees()
        fun onClickedRow(position : Int , employee : Employee)
        fun start()
    }
}