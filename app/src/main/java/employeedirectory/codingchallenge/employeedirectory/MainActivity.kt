package employeedirectory.codingchallenge.employeedirectory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import employeedirectory.codingchallenge.employeedirectory.data.EmployeeRoomDatabase
import employeedirectory.codingchallenge.employeedirectory.data.LocalDataSource
import employeedirectory.codingchallenge.employeedirectory.ui.main.MainFragment
import employeedirectory.codingchallenge.employeedirectory.utils.RunBlock

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        var presenter: PresenterImpl? = null
        val dao = EmployeeRoomDatabase.getDatabase(this, object : DatabaseCallback {
            override fun onCompleted() {
                RunBlock.getInstance().onMainThread { presenter?.loadAllEmployees() }
            }
        })
        val localDataSource = LocalDataSource(dao)
        val frag = MainFragment.newInstance()
        presenter = PresenterImpl(frag, localDataSource)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, frag)
                    .commitNow()
        }
    }

    interface DatabaseCallback {
        fun onCompleted()
    }
}
